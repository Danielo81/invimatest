package gov.co.invima.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Persona.class)
public abstract class Persona_ {

	public static volatile SingularAttribute<Persona, Character> estado;
	public static volatile SingularAttribute<Persona, String> segundoapellido;
	public static volatile SingularAttribute<Persona, Integer> idpersona;
	public static volatile SingularAttribute<Persona, String> primernombre;
	public static volatile SingularAttribute<Persona, String> numerodocumento;
	public static volatile SingularAttribute<Persona, String> primerapellido;
	public static volatile SingularAttribute<Persona, String> segundonombre;

}

