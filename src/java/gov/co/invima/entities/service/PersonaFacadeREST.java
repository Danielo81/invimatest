/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gov.co.invima.entities.service;

import gov.co.invima.entities.Persona;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author danielo
 */
@Stateless
@Path("gov.co.invima.entities.persona")
public class PersonaFacadeREST extends AbstractFacade<Persona> {

    @PersistenceContext(unitName = "InvimaPU")
    private EntityManager em;

    public PersonaFacadeREST() {
        super(Persona.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Persona entity) {
        super.create(entity);
    }
    
    @Path("gestionar")
    @POST
    public void gestionarPersona(Persona persona) {
        Persona gestionada = super.find(persona.getIdpersona());
        if(persona != null){
            super.edit(persona);
        }else{
            super.create(persona);
        }
    }
    
    @Path("persona")
    @POST
    public void gestionarDatosPersona(Persona persona) {
        Persona gestionada = super.find(persona.getIdpersona());
        if(persona != null){
            super.edit(gestionada);
        }
    }
    
    

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Persona entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }
    
    @DELETE
    @Path("{idPersona}")
    public void borrar(@PathParam("idPersona") Integer id) {
        Persona gestionada = super.find(id);
        if(gestionada != null){
            gestionada.setEstado('I');
            super.edit(gestionada);
        }
        
    }

    @GET
    @Path("{idPersona}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Persona find(@PathParam("idPersona") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Persona> findAll() {
        return super.findAll();
    }
    
    @GET
    @Path("{primerNombre}/{segundoNombre}/{idPersona}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void actualizarInfoParcial(@PathParam("primerNombre") String primerNombre, 
            @PathParam("segundoNombre") String segundoNombre,
            @PathParam("idPersona") Integer idPersona){
            Persona gestionada = super.find(idPersona);
            if (gestionada != null){
                gestionada.setPrimernombre(primerNombre);
                gestionada.setSegundonombre(segundoNombre);
                super.edit(gestionada);
            }
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Persona> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }
    
    @GET
    @Path("estado")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Persona> encontrarPorEstado(@PathParam("estado") Character estado) {
        return super.findByEstado(estado);
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
